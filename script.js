let originalObj = {
  name: 'Iryna',
  age: 30,
  hobbies: ['reading', 'traveling'],
};

let copy = cloneObject(originalObj);

function cloneObject(obj) {
  let copy = Array.isArray(obj) ? [] : {};
  for (let key in obj) {
    if (typeof obj[key] === 'object' && obj[key] !== null) {
      copy[key] = cloneObject(obj[key]);
    } else {
      copy[key] = obj[key];
    }
  }
  return copy;
}
console.log(copy);